<link rel="stylesheet" type="text/css" href="public/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="public/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="public/css/controller-home.css">

<script type="text/javascript" src="public/js/jquery-3.1.1.min.js"></script>
<body style="background: url(public/img/bg.png);">
	<?php
		if (!isset($_SESSION["userName"])){
			?>
			<div class="menu">
				<div class="menu-home">
					<a href="index.php"><img src="public/img/home.png"></a>
				</div>
				<ul class="menu-list">
					<li><a href="index.php"><center><i class="fa fa-home"></i><span>Trang chủ</span></center></a></li>
					<li><a href="index.php?controller=sangtac"><center><i class="fa fa-pencil"></i></center><span>Sáng tác</span></a></li>
					<li><a href="index.php?controller=convert"><center><i class="fa fa-book"></i></center><span>Convert</span></a></li>
					<li><a href="index.php?controller=timkiem"><center><i class="fa fa-search"></i><span>Tìm kiếm</span></center></a></li>
					<li><a href="index.php?controller=danhsach"><center><i class="fa fa-list"></i></center><span>Danh sách</span></a></li>
					<li><a href="index.php?controller=dangnhap"><center><i class="fa fa-sign-in"></i></center><span>Đăng nhập</span></a></li>
				</ul>
				<div class="menu-footer">
					<div class="info">
						<a href="index.php?controller=login"><i class="fa fa-heart"></i></a>
						<a href="index.php?controller=login"><i class="fa fa-cog"></i></a>
					</div>
					<a href="index.php"><img src="public/img/ln.png"></a>
				</div>
			</div>
		<?php
		}
		else{
			?>
			<?php
		}
		?>
	<div id="manga" style="position: absolute;left: 20%;top: 20%;width: 700px;background-color: white;">
		<h3 class="text-center">
			<a href=""><?php echo $arr['ln_name']?></a>
		</h3>
		<div id="info" style="width: 100%">
			<div id="img" style="width: 20%;float: left;margin: 10px">
				<img src="public/<?php echo $arr['link_img']?>" style="width: 100%">
			</div>
			<div id="info2" style="width: 75%;float: left;margin-top: 10px">
				<p><?php echo $arr['ln_describe']?></p>
				<div style="width: 100%;background-color: #e0e0e0">
					<?php for($i = 0;$i < 4;$i++) ?> <div><img src="public/<?php echo $chapter['link_img']?>"></div>
				</div>
			</div>
		</div>
		<div class="new_chapter"></div>
	</div>
</body>