<?php
	/**
	* 
	*/
	$host = "localhost";
	$name = "root";
	$pass = "";
	$db = "lightnovel";
	$link = mysqli_connect($host,$name,$pass,$db);
	mysqli_set_charset($link,"UTF8");
	class model{
		public function query($sql){
			global $link;
			mysqli_query($link,$sql);
		} 
		public function fetchAll($sql){
			global $link;
			$arr = array();
			$result = mysqli_query($link,$sql);
			while($row = mysqli_fetch_array($result)) $arr[] = $row;
			return $arr;
		}
		public function fetchOne($sql){
			global $link;
			$result = mysqli_query($link,$sql);
			return mysqli_fetch_array($result);
		}
	}
?>