var check = true;
$('#add').click(function(){
	if(!check) return false;
	check = false;
	setTimeout(function(){
		$('.alert').removeClass('danger').removeClass('safe');
		check = true;
	}, 3000);
	var quantity = parseInt($('#quantity').val()); //tra ve so luong trong form text
	if(isNaN(quantity)) {
		$('.alert').html("Chưa nhập số lượng sản phẩm <i class='fa fa-times'></i>").addClass('danger');
		return false;
	}
	$.ajax({
		url : "controller/cart.php",//lay du lieu o file cart
		type : "get",//phuong thuc la get
		data : {//du lieu gui len server
			reset : false,
			quantity : quantity,
			id_product : $('.saveid').attr('id')
		},
		dataType : "json",//kieu tra ve la json
		success : function(result) {//neu thanh cong
			if(typeof result['error'] == 'undefined') {//neu khong loi
				var cart = result['quantity'];
				$('#bell').text(cart);
				$('.alert').html("Đã thêm vào giỏ hàng của bạn <i class='fa fa-check'></i>").addClass('safe');
			} else {
				$('.alert').html(result['error'] + " <i class='fa fa-times'></i>").addClass('danger');
			}
		}
		    
	})
	return false;
})