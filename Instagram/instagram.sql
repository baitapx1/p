-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2017 at 11:12 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `instagram`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_interact` int(10) UNSIGNED NOT NULL,
  `id_noti` int(10) UNSIGNED NOT NULL,
  `message` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `id_interact`, `id_noti`, `message`, `date`) VALUES
(1, 2, 3, 'nice, you\'re a lucky man', '2017-09-23 00:00:00'),
(2, 1, 4, 'Really?', '2017-09-24 00:08:10'),
(3, 2, 5, 'Where did you take it?', '2017-09-24 02:06:00'),
(4, 6, 0, 'We went to SaPa lastweek', '2017-09-24 05:00:06'),
(5, 6, 0, 'It was awsome', '2017-09-24 14:33:40'),
(6, 2, 16, 'So lovely you two!', '2017-09-24 22:10:55'),
(7, 11, 18, 'who is she?', '2017-09-24 22:15:25'),
(8, 11, 19, 'i\'ve never seen her before', '2017-09-24 22:17:18'),
(9, 8, 0, 'just my friend in high school', '2017-09-24 22:19:50'),
(10, 12, 20, 'What kind of club is it?', '2017-09-24 22:24:36'),
(11, 13, 21, 'hey man, she is my sister, who are you?', '2017-09-24 22:26:35'),
(12, 14, 22, 'where is there?', '2017-09-24 22:28:19'),
(13, 9, 0, 'a programming club, wanna join?', '2017-09-24 22:30:34'),
(14, 9, 0, 'they\'ll help you a lot', '2017-09-24 22:32:05'),
(15, 4, 23, 'thong nhat park', '2017-09-25 18:59:30'),
(16, 15, 30, 'how could u do that to me?', '2017-09-26 09:40:17'),
(17, 15, 31, 'u make me so confused', '2017-09-26 09:41:14'),
(20, 8, 0, 'dont be mad at me', '2017-09-26 09:51:44'),
(21, 16, 0, '@tuan how do u know?', '2017-09-26 15:06:56'),
(22, 16, 0, '@duong tuan\'s right', '2017-09-26 15:39:40'),
(23, 6, 0, '@tuan@duy wanna join the trip next week', '2017-09-26 15:48:52');

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_following` int(10) UNSIGNED NOT NULL,
  `id_noti` int(10) UNSIGNED NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`id`, `id_user`, `id_following`, `id_noti`, `date`) VALUES
(2, 3, 1, 2, '2017-09-17 00:00:00'),
(4, 4, 2, 12, '2017-09-24 00:00:00'),
(5, 5, 1, 13, '2017-09-24 00:00:00'),
(7, 3, 2, 17, '2017-09-24 22:13:36'),
(9, 2, 1, 25, '2017-09-25 21:12:34'),
(11, 1, 3, 27, '2017-09-25 21:14:26'),
(13, 2, 3, 29, '2017-09-25 21:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `interact`
--

CREATE TABLE `interact` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_post` int(10) UNSIGNED NOT NULL,
  `love` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `interact`
--

INSERT INTO `interact` (`id`, `id_user`, `id_post`, `love`) VALUES
(1, 3, 1, 1),
(2, 2, 1, 1),
(3, 4, 1, 1),
(4, 2, 4, 1),
(5, 2, 3, 0),
(6, 1, 1, 1),
(7, 3, 4, 1),
(8, 2, 2, 1),
(9, 1, 3, 1),
(11, 3, 2, 0),
(12, 5, 3, 0),
(13, 5, 2, 0),
(14, 5, 4, 0),
(15, 4, 2, 0),
(16, 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mention`
--

CREATE TABLE `mention` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_comment` int(10) UNSIGNED NOT NULL,
  `id_noti` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mention`
--

INSERT INTO `mention` (`id`, `id_comment`, `id_noti`) VALUES
(1, 21, 32),
(2, 22, 33),
(3, 23, 34),
(4, 23, 35);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `noti_type` char(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `id_user`, `noti_type`) VALUES
(2, 1, 'f'),
(3, 1, 'c'),
(4, 1, 'c'),
(5, 1, 'c'),
(12, 2, 'f'),
(13, 1, 'f'),
(16, 1, 'c'),
(17, 2, 'f'),
(18, 2, 'c'),
(19, 2, 'c'),
(20, 1, 'c'),
(21, 2, 'c'),
(22, 1, 'c'),
(23, 1, 'c'),
(25, 1, 'f'),
(27, 3, 'f'),
(29, 3, 'f'),
(30, 2, 'c'),
(31, 2, 'c'),
(32, 2, 'm'),
(33, 5, 'm'),
(34, 2, 'm'),
(35, 3, 'm');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `link_img` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `id_user`, `link_img`, `date`) VALUES
(1, 1, 'img/img1.jpg', '2017-09-18 10:26:08'),
(2, 2, 'img/img2.jpg', '2017-09-16 13:19:13'),
(3, 1, 'img/img3.jpg', '2017-09-17 16:48:10'),
(4, 1, 'img/img4.jpg', '2017-09-18 05:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `link_avatar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `noti` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `link_avatar`, `noti`) VALUES
(1, 'phan', 'phan', 'img/img1.jpg', 0),
(2, 'tuan', '12345', 'img/img2.jpg', 0),
(3, 'duy', '12345', 'img/img0.jpg', 0),
(4, 'trang', '12345', 'img/img0.jpg', 0),
(5, 'duong', '12345', 'img/img0.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interact`
--
ALTER TABLE `interact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mention`
--
ALTER TABLE `mention`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `follow`
--
ALTER TABLE `follow`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `interact`
--
ALTER TABLE `interact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `mention`
--
ALTER TABLE `mention`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
